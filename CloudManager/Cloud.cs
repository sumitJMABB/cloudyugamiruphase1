﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace CloudManager
{
    public class Cloud
    {
        
        public class PatientRecord
        {
            public string UniqueID = "not set";
            private string stLut = "";
            public Stamp LUT;
            public PatientRecord()
            {
                
                UniqueID = null;
                stLut = null;
            }
            
            public PatientRecord(DataRow rowRecord)
            {
                initCloudSystem.CreateCloudDatabase();
                if(rowRecord==null || rowRecord[0]==null||rowRecord[1]==null || rowRecord[0].ToString().Length==0 || rowRecord[1].ToString().Length==0)
                {
                    this.UniqueID = null;
                    this.stLut = null;
                }
                //if (rowRecord == null || this.UniqueID.Length == 0 || this.stLut.Length == 0)
                //{
                //    this.UniqueID = null;
                //    this.stLut = null;
                //}
                else
                {
                    this.UniqueID = rowRecord["UniqueId"].ToString().Trim();
                    this.stLut = rowRecord["lut"].ToString().Trim();
                    this.LUT = new Stamp(stLut);
                }


            }
            public override string ToString()
            {
                return this.LUT.ToString(); 
            }

        }
        //public static PatientRecord AppDBMax()
        //{
        //    //initCloudSystem.CreateCloudDatabase();
        //    PatientRecord p1 = new PatientRecord()

        //    return p1;
        //}
        
    }
    public class AppDB
    {
        
        public class PatientRecord
        {
            public string UniqueID = "not set";
            private string stLut = "";
            public Stamp LUT;
            public PatientRecord()
            {

                UniqueID = null;
                stLut = null;
            }

            public PatientRecord(DataRow rowRecord)
            {
                
                if (rowRecord == null || rowRecord[0] == null || rowRecord[1] == null || rowRecord[0].ToString().Length == 0 || rowRecord[1].ToString().Length == 0)
                {
                    this.UniqueID = null;
                    this.stLut = null;
                }
                //if (rowRecord == null || this.UniqueID.Length == 0 || this.stLut.Length == 0)
                //{
                //    this.UniqueID = null;
                //    this.stLut = null;
                //}
                else
                {
                    this.UniqueID = rowRecord["UniqueId"].ToString().Trim();
                    this.stLut = rowRecord["lut"].ToString().Trim();
                    this.LUT = new Stamp(stLut);
                }


            }
            public override string ToString()
            {
                return this.LUT.ToString();
            }

        }
        //public static PatientRecord AppDBMax()
        //{
        //    //initCloudSystem.CreateCloudDatabase();
        //    PatientRecord p1 = new PatientRecord()

        //    return p1;
        //}

    }

    public class Stamp
    {
        public int year = -1;
        public int month = -1;
        public int day = -1;
        public int hour = -1;
        public int min = -1;
        public int second = -1;
        private int value = -1;

        /// <summary>
        ///  Default to Current timestamp
        /// </summary>
        public Stamp()
        {
            string[] s = Stamp.CurrentStamp().Split('-');
            string y = s[2];
            string m = s[1];
            string d = s[0];
            string h = s[3];
            string n = s[4];
            string sec = s[5];

            year = Convert.ToInt16(y);
            month = Convert.ToInt16(m);
            day = Convert.ToInt16(d);
            hour = Convert.ToInt16(h);
            min = Convert.ToInt16(n);
            second = Convert.ToInt16(sec);
        }

        public Stamp(string stampText)
        {
            string[] s = stampText.Split('-');
            string y = s[2];
            string m = s[1];
            string d = s[0];
            string h = s[3];
            string n = s[4];
            string sec = s[5];

            year = Convert.ToInt16(y);
            month = Convert.ToInt16(m);
            day = Convert.ToInt16(d);
            hour = Convert.ToInt16(h);
            min = Convert.ToInt16(n);
            second = Convert.ToInt16(sec);
        }
        public static Stamp GetGreater(Stamp s1, Stamp s2)
        {
            if (s1.year > s2.year)
            {
                return s1;
            }
            else if (s1.year == s2.year)
            {
                if (s1.year == s2.year)
                {
                    if (s1.month > s2.month)
                    {
                        return s1;
                    }
                    else if (s1.month == s2.month)
                    {
                        if (s1.day > s2.day)
                        {
                            return s1;
                        }
                        else if (s1.day == s2.day)
                        {
                            if (s1.hour > s2.hour)
                            {
                                return s1;
                            }
                            else if (s1.hour == s2.hour)
                            {

                                if (s1.min > s2.min)
                                {
                                    return s1;
                                }
                                else if (s1.min == s2.min)
                                {
                                    if (s1.second > s2.second)
                                    {
                                        return s1;
                                    }
                                    else if (s1.second == s2.second)
                                    {
                                        return s1;
                                    }
                                    else if (s1.second < s2.second)
                                    {
                                        return s2;
                                    }
                                }
                                else if (s1.min < s2.min)
                                {
                                    return s2;
                                }
                            }
                            else if (s1.hour < s2.hour)
                            {
                                return s2;
                            }

                        }
                        else if (s1.day < s2.day)
                        {
                            return s2;
                        }
                    }
                    else if (s1.month < s2.month)
                    {
                        return s2;
                    }
                }
            }
            else if (s1.year < s2.year)
            {
                return s2;
            }

            return s1;
        }
        /// <summary>
        /// returns in format y-m-d-h-mi-s
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.year.ToString() + "-" +
                this.month.ToString() + "-" +
                this.day.ToString() + "-" +
                this.hour.ToString() + "-" +
                this.min.ToString() + "-" +
                this.second.ToString();

        }
        public int Value()
        {
            value = this.year + this.month + this.day + this.hour + this.min + this.second;
            return value;
        }
        public static string CurrentStamp()
        {
            return DateTime.Now.Day.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Hour + "-" + DateTime.Now.Minute.ToString() + "-" + DateTime.Now.Second;
        }

        public static bool operator ==(Stamp s1, Stamp s2)
        {
            if (s1.year == s2.year &&
                s1.month == s2.month &&
                s1.day == s2.day &&
                    s1.hour == s2.hour &&
                s1.min == s2.min &&
                s1.second == s2.second)
                return true;
            else
                return false;

        }
        public static bool operator !=(Stamp s1, Stamp s2)
        {
            if (s1.year != s2.year ||
                s1.month != s2.month ||
                s1.day != s2.day ||
                s1.hour != s2.hour ||
                s1.min != s2.min ||
                s1.second != s2.second)
                return true;
            else
                return false;

        }
        public static bool operator >(Stamp s1, Stamp s2)
        {
            Stamp sg = Stamp.GetGreater(s1, s2);
            if(sg==s1)
            {
                return true;//equal not greater
            }
            else
            {
                return false;
            }
            //else if(sg==s2)
            //{
            //    return
            //}
            //if (s1 == s2)
            //{
            //    throw new Exception("Both are equal");
            //}
            //if (s1.year >= s2.year &&
            //    s1.month >= s2.month &&
            //    s1.day >= s2.day &&
            //        s1.hour >= s2.hour &&
            //    s1.min >= s2.min &&
            //    s1.second >= s2.second)
            //    return true;
            //else
            //    return false;

        }
        public static bool operator <(Stamp s1, Stamp s2)
        {
            if (s1 == s2)
            {
                throw new Exception("Both are equal");
            }
            if (s1.year <= s2.year &&
                s1.month <= s2.month &&
                s1.day <= s2.day &&
                    s1.hour <= s2.hour &&
                s1.min <= s2.min &&
                s1.second <= s2.second)
                return true;
            else
                return false;

        }
    }

}
