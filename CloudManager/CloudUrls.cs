﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudManager
{
    public static class CloudUrls
    {
        public static string PatientDetailsSendToUrl = @"http://gs-demo.jma.website/apioauthdata/index.php/cloudData/uploadpatient_data";           
        public static string FrontBodyPositionKneeDownSendToUrl = @"http://gs-demo.jma.website/apioauthdata/index.php/cloudData/uploadkneedown_data";
        public static string FrontBodyPositionStandingSendToUrl = @"http://gs-demo.jma.website/apioauthdata/index.php/cloudData/uploadstanding_data";
        public static string SideBodyPositionSendToUrl = @"http://gs-demo.jma.website/apioauthdata/index.php/cloudData/uploadsidebody_data";

        public static string PatientDetailsGetFromUrl = @"http://gs-demo.jma.website/apioauthdata/index.php/cloudData/downloadpatient_data";
        public static string FrontBodyPositionKneeDownGetFromUrl = @"http://gs-demo.jma.website/apioauthdata/index.php/cloudData/downloadkneedown_data";
        public static string FrontBodyPositionStandingGetFromUrl = @"http://gs-demo.jma.website/apioauthdata/index.php/cloudData/downloadstanding_data";
        public static string SideBodyPositionGetFromUrl = @"http://gs-demo.jma.website/apioauthdata/index.php/cloudData/downloadsidebody_data";
        

    }
}
