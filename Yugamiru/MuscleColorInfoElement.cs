﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yugamiru
{
    public class MuscleColorInfoElement
    {

        // ‹Ø“÷•\Ž¦Fî•ñ‚Ì—v‘f.
        int m_iMuscleID;
        int m_iMuscleColorID;

        public MuscleColorInfoElement()
            {

    m_iMuscleID = Constants.MUSCLEID_NONE ;

    m_iMuscleColorID = Constants.MUSCLECOLORID_NONE ;
        
        }

        public MuscleColorInfoElement( int iMuscleID, int iMuscleColorID )
        {
            m_iMuscleID = iMuscleID;
            m_iMuscleColorID = iMuscleColorID;
        
        }



        public int GetMuscleID( ) 
{
	return m_iMuscleID;
}

public int GetMuscleColorID() 
{
	return m_iMuscleColorID;
}

public void SetMuscleColorID(int iNewValue)
{
    m_iMuscleColorID = iNewValue;
}

    }
}
